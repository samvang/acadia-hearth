<?php

function theme_enqueue_styles() {
//	wp_enqueue_style( 'tippy-css', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_scripts() {
	wp_enqueue_script( 'tippy-js', 'https://unpkg.com/tippy.js@3/dist/tippy.all.min.js', array(), false, true );
//	wp_enqueue_script( 'popper-js', 'https://unpkg.com/popper.js/dist/umd/popper.min.js', array(), false, true );
//	wp_enqueue_script( 'tooltip-js', 'https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js', array( 'popper-js' ), false, true );
//	wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() . '/scripts.js', array( 'jquery', 'popper-js', 'tooltip-js' ), false, true );
	wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() . '/scripts.js', array( 'jquery', 'tippy-js' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}

add_action( 'after_setup_theme', 'avada_lang_setup' );

/**
 * WordPress Setup
 */
function ah_wordpress_setup() {
	add_image_size( 'ah_component_thumb', 100, 100, true );
	add_image_size( 'ah_component_thumb_resize', 100, 100, false );
}

add_action( 'init', 'ah_wordpress_setup' );

/**
 * Acadia Hearth - WooCommerce
 */

// Block access to Product Cat archive
function ah_disable_product_category_archive() {
	if ( is_product_taxonomy() ) {
		global $wp_query;
		$wp_query->set_404();
	}
}

add_action( 'template_redirect', 'ah_disable_product_category_archive' );

// Redirect all Product Cat requests to Products page
function ah_product_category_term_link( $termlink, $term, $taxonomy ) {
	if ( 'product_cat' === $taxonomy ) {
		$termlink = home_url( '/products/' );
	}

	return $termlink;
}

add_filter( 'term_link', 'ah_product_category_term_link', 10, 3 );

// Customize Product Card
function ah_customize_shop_loop_item_hooks() {
	add_action( 'woocommerce_after_shop_loop_item', 'ah_woocommerce_after_shop_loop_item_view_product', 10 );
}

add_action( 'wp', 'ah_customize_shop_loop_item_hooks' );

// Customize Product Single
function ah_customize_single_product_hooks() {
	global $avada_woocommerce;

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
//	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_single_product_summary', array( $avada_woocommerce, 'stock_html' ), 10 );
	remove_action( 'woocommerce_single_product_summary', array( $avada_woocommerce, 'add_product_border' ), 19 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 20 );
//	add_action( 'woocommerce_single_product_summary', 'woocommerce_product_description_tab', 10 );

//	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
}

add_action( 'wp', 'ah_customize_single_product_hooks' );

function ah_remove_single_product_description_title( $title ) {
	return '';
}

add_filter( 'woocommerce_product_description_heading', 'ah_remove_single_product_description_title', 10, 1 );

function ah_woocommerce_after_shop_loop_item_view_product() {
	global $post;
	$link = get_permalink();
	echo "<a class='view-product' href='$link'>View Product <span class='arrow'>&rarr;</span></a>";
}

function ah_woocommerce_product_tabs( $tabs ) {

	unset( $tabs['description'] );

	if ( get_field( 'product_features' ) ) {
		$tabs['ah_features'] = [
			'title'    => __( 'Features', 'acadiahearth' ),
			'priority' => 10,
			'callback' => 'ah_product_features_tab_callback',
		];
	}

	if ( get_field( 'component_groups' ) ) {
		$tabs['ah_components'] = [
			'title'    => __( 'Components', 'acadiahearth' ),
			'priority' => 11,
			'callback' => 'ah_product_components_tab_callback',
		];
	}

	return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'ah_woocommerce_product_tabs' );

function ah_product_features_tab_callback() {
	printf( "<h3>%s</h3>%s",
		__( 'Features', 'acadiahearth' ),
		wp_kses_post( get_field( 'product_features' ) )
	);
}

function ah_product_components_tab_callback() {
	ob_start();
	get_template_part( 'woocommerce/templates/single-product/tab-components' );
	$html = ob_get_clean();
	echo $html;
}

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function ( $size ) {
	return array(
		'width'  => 200,
		'height' => 200,
		'crop'   => 0,
	);
} );

/**
 * Acadia Hearth - WooCommerce Composite Products
 */
add_action( 'wp_print_scripts', 'deactivate_wc_cp_scripts', 100 );
add_action( 'wp_print_footer_scripts', 'deactivate_wc_cp_scripts', 100 );
function deactivate_wc_cp_scripts() {
	wp_dequeue_style( 'wc-composite-single-css' );
	wp_deregister_style( 'wc-composite-single-css' );

	wp_dequeue_style( 'wc-composite-css' );
	wp_deregister_style( 'wc-composite-css' );
}

function ah_composite_component_option_data( $data, $composite_option ) {
	$data['is_selected'] = false;

	return $data;
}

add_filter( 'woocommerce_composite_component_option_data', 'ah_composite_component_option_data', 10, 2 );

function ah_composite_component_loop_columns( $cols, $id, $composite ) {
	return 5;
}

add_filter( 'woocommerce_composite_component_loop_columns', 'ah_composite_component_loop_columns', 10, 3 );

function ah_composite_component_option_image_size( $size, $product ) {
	return array( 100, 100 );
}

add_filter( 'woocommerce_composite_component_option_image_size', 'ah_composite_component_option_image_size', 10, 2 );
