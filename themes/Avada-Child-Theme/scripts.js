;( function( $, window, document, undefined ) {

	var $jsComponentTooltip = $( '.js-component-tooltip' );

	$jsComponentTooltip.each( function( i, el ) {
		var id = el.getAttribute( 'data-product-id' );
		const template = document.getElementById( 'tooltip-content-id-' + id );
		tippy( el, {
			animation: 'scale',
			duration: 0,
			arrow: true,
			content: template.innerHTML,
		} );
	} );

} )( jQuery, window, document );