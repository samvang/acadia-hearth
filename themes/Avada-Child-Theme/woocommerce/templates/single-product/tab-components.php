<?php
/**
 * Filename component-group.php
 *
 * @package acadiahearth
 * @author  Peter Toi <peter@petertoi.com>
 */

$groups = get_field( 'component_groups' );
?>
<div class="ah-component-groups">
	<?php foreach (
		$groups

		as $group
	) : ?>
		<?php
		if ( ! ( $group['product_cat'] instanceof \WP_Term ) ) {
			continue;
		}
		?>
		<div class="ah-component-group">
			<h3 class="ah-component-group__title"><?php echo esc_html( $group['title'] ); ?></h3>
			<?php

			$products = wc_get_products( [
				'status'   => 'publish',
				'limit'    => 999,
				'category' => $group['product_cat']->slug,
				'orderby'  => 'title',
				'order'    => 'ASC',
			] );
			?>

			<?php
			/**
			 * This block is designed to show the components as image tiles
			 * but the images are lacking. I'm not confident that the design
			 * supports the variety of components sizes/detail.
			 *
			 * In place, I am simply listing the component titles.
			 */
			?>
			<?php if ( $products ) : ?>
				<ul class="ah-component-group__components ah-clearfix">
					<?php
					/**
					 * @var \WC_Product $product
					 */
					?>
					<?php foreach ( $products as $product ) : ?>
						<li class="ah-component-group__component">
							<div class="js-component-tooltip" data-product-id="<?php echo $product->get_id(); ?>">
								<?php echo $product->get_image('ah_component_thumb_resize', ['width' => 100, 'height' => 100]); ?>
								<div id="tooltip-content-id-<?php echo $product->get_id(); ?>" style="display: none;">
									<strong><?php echo $product->get_title(); ?></strong>
									<?php if ( $product->is_type( 'variable' ) )  : ?>

										<?php
										$variations = new WP_Query( [
											'post_type'   => 'product_variation',
											'post_status' => array( 'publish' ),
											'numberposts' => - 1,
											'post_parent' => $product->get_id(),
										] );
										?>

										<?php if ( $variations->have_posts() ) : ?>
											<ul class="component__variations">
												<?php foreach ( $variations->posts as $variation ) : ?>
													<li class="component__variation"><?php echo $variation->post_title; ?></li>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>

									<?php endif; ?>
								</div>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>


			<?php if ( false && $products ) : ?>
				<ul class="ah-component-group__components-list ah-clearfix">
					<?php
					/**
					 * @var \WC_Product $product
					 */
					?>
					<?php foreach ( $products as $product ) : ?>
						<li class="ah-component-group__component-item"><?php echo $product->get_title(); ?></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
</div>
