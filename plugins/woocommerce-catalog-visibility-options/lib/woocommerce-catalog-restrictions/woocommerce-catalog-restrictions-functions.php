<?php

function woocommerce_catalog_restrictions_country_input( $value = '', $args = '' ) {
	global $woocommerce;

	$key = 'location';

	$args = wp_parse_args( $args, array(
		'class'       => array(),
		'id'          => 'location',
		'label_class' => array(),
		'label'       => __( 'Select your location', 'wc_catalog_restrictions' )
	) );

	$field = '<label for="' . $key . '" class="' . implode( ' ', $args['label_class'] ) . '">' . $args['label'] . '</label>
              <select name="' . $key . '" id="' . $key . '" class="country_to_state ' . implode( ' ', $args['class'] ) . '">';


	if ( apply_filters( 'woocommerce_catalog_restrictions_locations_include_states', get_option( '_wc_restrictions_locations_type' ) == 'states' ) ) {

		$field .= '<option value="">' . __( 'Select your country / state&hellip;', 'wc_catalog_restrictions' ) . '</option>';

		$all_states = WC()->countries->get_allowed_country_states();
		foreach ( $woocommerce->countries->get_allowed_countries() as $ckey => $cvalue ) {

			$states = isset( $all_states[ $ckey ] ) ? $all_states[ $ckey ] : false;
			if ( $states && ! empty( $states ) ) {
				$field .= '<optgroup label="' . __( $cvalue, 'woocommerce' ) . '">';

				foreach ( $states as $skey => $state ) {
					$cs_value = $ckey . $skey;
					$selected = '';
					if ( is_array( $value ) ) {
						$selected = in_array( $cs_value, $value ) ? 'selected="selected"' : '';
					} else {
						$selected = $cs_value == $value ? 'selected="selected"' : '';
					}

					$field .= '<option value="' . $cs_value . '" ' . $selected . '>' . __( $state, 'woocommerce' ) . '</option>';
				}
				$field .= '</optgroup>';
			} else {
				$selected = '';
				if ( is_array( $value ) ) {
					$selected = in_array( $ckey, $value ) ? 'selected="selected"' : '';
				} else {
					$selected = $ckey == $value ? 'selected="selected"' : '';
				}

				$field .= '<option value="' . $ckey . '" ' . $selected . '>' . __( $cvalue, 'woocommerce' ) . '</option>';

			}

		}
	} else {
		$field .= '<option value="">' . __( 'Select your country&hellip;', 'wc_catalog_restrictions' ) . '</option>';

		foreach ( $woocommerce->countries->get_allowed_countries() as $ckey => $cvalue ) {
			$selected = '';
			if ( is_array( $value ) ) {
				$selected = in_array( $ckey, $value ) ? 'selected="selected"' : '';
			} else {
				$selected = $ckey == $value ? 'selected="selected"' : '';
			}

			$field .= '<option value="' . $ckey . '" ' . $selected . '>' . __( $cvalue, 'woocommerce' ) . '</option>';
		}
	}


	$field .= '</select>';

	echo $field;
}

function woocommerce_catalog_restrictions_country_multiselect_options( $selected_values = '', $escape = false ) {

	if ( apply_filters( 'woocommerce_catalog_restrictions_locations_include_states', get_option( '_wc_restrictions_locations_type' ) == 'states' ) ) {
		$all_states = WC()->countries->get_allowed_country_states();
		$countries  = WC()->countries->get_allowed_countries();
		foreach ( $countries as $ckey => $cval ) {
			$states = isset( $all_states[ $ckey ] ) ? $all_states[ $ckey ] : false;
			if ( $states && ! empty( $states ) ) {
				echo '<optgroup label="' . esc_attr( $cval ) . '">';
				foreach ( $states as $skey => $sval ) {
					$cs_value = $ckey . $skey;
					echo '<option ' . selected( in_array( $cs_value, $selected_values ), true, false ) . 'value="' . $cs_value . '">' . $sval . '</option>';
				}
				echo '</optgroup>';
			}else {
				echo '<option ' . selected( in_array( $ckey, $selected_values ), true, false ) . ' value="' . $ckey . '">' . ( $escape ? esc_js( $cval ) : $cval ) . '</option>';
			}
		}

	} else {

		$countries = WC()->countries->get_allowed_countries();
		foreach ( $countries as $key => $val ) {
			echo '<option ' . selected( in_array( $key, $selected_values ), true, false ) . ' value="' . $key . '">' . ( $escape ? esc_js( $val ) : $val ) . '</option>';
		}
	}
}